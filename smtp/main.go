package main

import (
	"fmt"
	"log"
	"net/mail"
	"net/smtp"
	"os"

	"github.com/BurntSushi/toml"
	email "gopkg.in/jordan-wright/email.v2"
)

type config struct {
	Mail struct {
		Subject     string
		Body        string
		Attachments []string
		From        struct {
			Address string
			Name    string
		}
		To struct {
			Address string
			Name    string
		}
		Method string
		SMTP   struct {
			Address  string
			Password string
			Port     int
			UserName string `toml:"user_name"`
		}
	}
}

func (config config) mailFrom() string {
	from := mail.Address{
		Name:    config.Mail.From.Name,
		Address: config.Mail.From.Address}

	return from.String()
}

func (config config) mailTo() string {
	to := mail.Address{
		Name:    config.Mail.To.Name,
		Address: config.Mail.To.Address}

	return to.String()
}

func main() {
	config, err := loadConfig(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	auth := smtp.PlainAuth(
		"",
		config.Mail.SMTP.UserName,
		config.Mail.SMTP.Password,
		config.Mail.SMTP.Address)

	msg, err := dummyMail(config)
	if err != nil {
		log.Fatal(err)
	}

	err = smtp.SendMail(
		fmt.Sprintf("%s:%d", config.Mail.SMTP.Address, config.Mail.SMTP.Port),
		auth,
		config.mailFrom(),
		[]string{config.mailTo()},
		msg)
	if err != nil {
		log.Fatal(err)
	}
}

func dummyMail(config *config) ([]byte, error) {
	message := email.NewEmail()
	message.From = config.mailFrom()
	message.To = []string{config.mailTo()}
	message.Subject = config.Mail.Subject
	for _, a := range config.Mail.Attachments {
		_, err := message.AttachFile(a)
		if err != nil {
			log.Fatal(err)
		}
	}

	// err = message.Send(
	// 	fmt.Sprintf("%s:%d", config.Mail.SMTP.Address, config.Mail.SMTP.Port),
	// 	smtp.PlainAuth(
	// 		"",
	// 		config.Mail.SMTP.UserName,
	// 		config.Mail.SMTP.Password,
	// 		config.Mail.SMTP.Address))

	message.Text = []byte(config.Mail.Body)

	return message.Bytes()
}

func loadConfig(path string) (*config, error) {
	var config *config

	_, err := toml.DecodeFile(path, &config)

	return config, err
}
