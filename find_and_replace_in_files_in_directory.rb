# Find and replace in files in directory.
# Released under the MIT License https://opensource.org/licenses/MIT

DIR = '/home/danil/.emacs.d/recipes'.freeze

Dir.foreach(DIR) do |item|
  next if item == '.' || item == '..'

  path = "#{DIR}/#{item}"
  text = File.read(path)

  pattern = /\(my-init--hook/

  next unless text =~ pattern
  x = File.basename(item, '.*')

  replacement =
    %[(add-hook 'after-init-hook '#{x})\n\n(defun #{x} ()\n  "Init."\n]

  File.open(path, 'w') { |f| f.write(text.gsub(pattern, replacement)) }

  puts item
end
