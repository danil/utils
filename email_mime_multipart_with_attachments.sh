#!/bin/bash

set -e

# Send email with attachment(s) (mime multipart).
# Based on
# <http://backreference.org/2013/05/22/send-email-with-attachments-from-script-or-command-line/>
# Released under the MIT License https://opensource.org/licenses/MIT
# Example of usage:
# `./email_mime_multipart_with_attachments.sh | /usr/sbin/sendmail -t -oi`

get_mimetype(){
    # warning: assumes that the passed file exists
    file --mime-type "$1" | sed 's/.*: //'
}

from="danil@kutkevich.org"
to="danil@kutkevich.org"
subject="Hello"
boundary=$(uuidgen)
body="Hello, World!"
html_body="<!doctype html>
<html>
    <head></head>
    <body>
        <p><strong>Hello</strong>, <em>World</em>!</p>
    </body>
</html>"

declare -a attachments
attachments=(
    "${BASH_SOURCE%/*}/fixtures/image.jpg"
    "${BASH_SOURCE%/*}/fixtures/test.pdf"
)

# Build headers
{

    printf '%s\n' "From: $from
To: $to
Subject: $subject
MIME-Version: 1.0"

    # If plain text mail with attachments then content type "multipart/mixed"
    # if html mail with attachments then content type "multipart/alternative".
    if [ -z "$html_body" ]; then
        printf '%s\n' "Content-Type: multipart/mixed; boundary=\"$boundary\""
    else
        printf '%s\n' "Content-Type: multipart/alternative; boundary=\"$boundary\""
    fi

    printf '%s\n' "--${boundary}
Content-Type: text/plain; charset=\"utf-8\"; format=\"fixed\"
Content-Transfer-Encoding: quoted-printable
Content-Disposition: inline

$body"

    if [ ! -z "$html_body" ]; then
        printf '%s\n' "--${boundary}
Content-Type: text/html; charset=\"utf-8\"
Content-Transfer-Encoding: quoted-printable
Content-Disposition: inline

$html_body"
    fi

    # now loop over the attachments, guess the type
    # and produce the corresponding part, encoded base64
    for file in "${attachments[@]}"; do

        if [ ! -f "$file" ]; then
            echo "Warning: attachment $file not found, skipping" >&2
            continue
        fi

        mimetype=$(get_mimetype "$file")

        printf '%s\n' "--${boundary}
Content-Type: $mimetype
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename=\"$(basename $file)\"
"
        base64 "$file"
        echo
    done

    # print last boundary with closing --
    printf '%s\n' "--${boundary}--"

}
