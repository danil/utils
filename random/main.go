package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	fmt.Println(random(1, 3))
}

func random(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(max-min) + min
}
